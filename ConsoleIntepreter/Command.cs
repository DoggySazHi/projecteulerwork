﻿using System;
using ProjectEuler.ConsoleIntepreter.ConsoleHandler;

namespace ProjectEuler.ConsoleIntepreter
{
    public class Command
    {
        public void PrintLine(string text)
        => Console.WriteLine(text);
        public void PrintLine(string text, ConsoleColor color)
        => ColorConsole.WriteLine(text, color);
        [Obsolete("Use ConsoleColor instead, less likely" +
                  " to cause errors if the color doesn't exist.")]
        public void PrintLine(string text, string color)
        => ColorConsole.WriteLine(text, color);


    }
}
