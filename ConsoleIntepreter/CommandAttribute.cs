﻿using System;

namespace ProjectEuler.ConsoleIntepreter
{
	public class CommandAttribute : Attribute
    {
        public string command { get; set; }
        public bool isActive { get; set; }
        public string version { get; set; }

        public CommandAttribute(string command)
        {
            this.command = command;
            isActive = true;
            version = "Unknown";
        }

        public CommandAttribute(string command, string version)
        {
            this.command = command;
            isActive = true;
            this.version = version;
        }
        public CommandAttribute(string command, bool isActive, string version)
        {
            this.command = command;
            this.isActive = isActive;
            this.version = version;
        }
	}
}
