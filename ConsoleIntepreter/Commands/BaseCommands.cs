﻿using System;

namespace ProjectEuler.ConsoleIntepreter.Commands
{
    public class BaseCommands : Command
    {
        [Command("setup", "1.0")]
        public void Setup()
        {
            PrintLine("Pre-caching...", ConsoleColor.Cyan);
            Tester.LoadProblems();
        }
        [Command("exit", "1.0")]
        public void Exit()
        {
            Environment.Exit(0);
        }
        [Command("help", "1.0")]
        public void Help()
        {
            Console.WriteLine("Short list:\n" +
                              "getquestion # - Print the question for the problem.\n" +
                              "test # - Test the problem with code.\n" +
                              "test # # - Test the problem, however with a forced solution. This is not recommended, intended for testing.\n" +
                              "test all - Test every single problem known.");
        }
        [Command("?", "1.0")]
        public void HelpAlias()
            => Help();

        [Command("getquestion")]
        public void GetQuestion(string input)
        {
            bool get = int.TryParse(input, out int inpu);
            int num = Tester.Problems.Count + 1;
            if (num - 1 == 0)
            {
                Console.WriteLine("Seems like no questions are loaded; please wait.");
                Setup();
                num = Tester.Problems.Count + 1;
            }
            if (!get || inpu > num || inpu < 1)
            {
                Console.WriteLine($"Your problem is out of bounds! Allowed values: 1 - {num}");
                return;
            }
            Console.Clear();
            Console.WriteLine($"Question {inpu}");
            Console.WriteLine("=========\n");
            int counter = 0;
            foreach (string output in Tester.Problems.Keys)
            {
                counter++;
                if (counter == inpu)
                {
                    Console.WriteLine(output);
                    return;
                }
            }
        }
        [Command("test")]
        public void Test(string input, string problem)
            => Tester.Test(input, problem);
        [Command("test")]
        public void Test2(string input)
            => Tester.Test(input);
    }
}