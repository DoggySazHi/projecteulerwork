﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Timers;

namespace ProjectEuler.ConsoleIntepreter.Commands
{
    public static class Tester
    {
        public static Dictionary<string, string> Problems = new Dictionary<string, string>();

        public static void Test(string problem)
        {
            if (!int.TryParse(problem, out int result))
            {
                Console.WriteLine("Seems like that's not a problem number.");
                return;
            }
            if (result < 1 || result > Problems.Count)
            {
                Console.WriteLine($"That is out of bounds! It must be between 1 and {Problems.Count}.");
                return;
            }
            string output = Execute(result);
            Test(output, problem);
        }

        public static void Test<T>(T input, string problem)
        {
            if (problem == "")
                return;
            using (var md5 = MD5.Create())
            {
                string inputString = input.ToString();
                if (!Int32.TryParse(problem, out int result))
                {
                    Console.WriteLine("Seems like that's not a problem number.");
                    return;
                }
                if (result < 1 || result > Problems.Count)
                {
                    Console.WriteLine($"That is out of bounds! It must be between 1 and {Problems.Count}.");
                    return;
                }
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(inputString);

                var inputHash = BitConverter.ToString(md5.ComputeHash(inputBytes)).Replace("-", "").ToLower();
                if (inputHash == Problems.Values.ElementAt(result - 1))
                {
                    Console.WriteLine("Nice job, that answer is correct.");
                    Console.WriteLine("Answer was " + inputString);
                }
                else
                    Console.WriteLine($"Sorry, but that's wrong. Got {inputHash} but supposed to be {Problems.Values.ElementAt(result)}");
            }
        }

        public static void LoadProblems()
        {
            if (Problems.Count != 0)
            {
                Console.WriteLine("[ERROR] Already precached!");
                return;
            }
            string source = @"project_euler.txt";
            string answerdivider = "Answer: ";

            string curLine = "";
            int count = 0;
            bool readingProblem = false;
            string problem = "";

            if (!File.Exists(source))
            {
                Console.WriteLine("[WARNING] Source file not found; downloading one right now!");
                using (WebClient wc = new WebClient())
                    wc.DownloadFile("http://kmkeen.com/local-euler/project_euler.txt", "project_euler.txt");
            }

            int max = CountProblems();

            Timer tm = new Timer
            {
                AutoReset = true,
                Interval = 100,
                Enabled = true
            };
            tm.Elapsed += (sender, e) => LoadingBar(max, false, count);

            using (StreamReader sr = new StreamReader(source))
            {
                String prevLine = sr.ReadLine();
                while ((curLine = sr.ReadLine()) != null)
                {
                    //problem without case lol, blem -> problem
                    if (curLine.Contains('=') && prevLine.Contains("blem"))
                    {
                        readingProblem = true;
                    }
                    else if (curLine.Contains(answerdivider))
                    {
                        readingProblem = false;
                        count++;
                        if (problem != "")
                        {
                            Problems.Add(problem.TrimStart('=').TrimStart('\n').TrimEnd('\n').Trim().TrimEnd('\n'), curLine.Substring(11));
                            problem = "";
                        }
                    }

                    if (readingProblem)
                        problem += curLine + "\n";
                    prevLine = curLine;
                }
            }
            Console.WriteLine(Problems.Count);
            Console.WriteLine("Done!");
            tm.Dispose();
        }

        public static int CountProblems()
        {
            string source = @"project_euler.txt";
            string answerdivider = "Answer: ";

            string curLine = "";
            int lineNum = 0;

            using (StreamReader sr = new StreamReader(source))
            {
                while ((curLine = sr.ReadLine()) != null)
                {
                    if (curLine.Contains(answerdivider))
                        lineNum++;
                }
            }
            Console.WriteLine(lineNum);
            return lineNum;
        }

        public static void LoadingBar(int max, bool searching = false, int current = 0)
        {
            Console.SetCursorPosition(0, Console.CursorTop);
            int size = max.ToString().Length + current.ToString().Length + 3;
            Console.Write($"{current}/{max}");
            Console.Write('[');
            for (int i = size; i < Console.LargestWindowWidth - 1; i++)
            {
                if (i / (Console.LargestWindowWidth - 2 - size) < current / max)
                    Console.Write('|');
                else
                    Console.Write(' ');
            }
            Console.Write(']');
        }

        static string Execute(int problem)
        {
            if (problem < 1 || problem > Problems.Count)
                return null;
            var types = from c in Assembly.GetExecutingAssembly().GetTypes()
                        where c.IsClass
                        select c;
            /*
            foreach (var type in types)
            {
                foreach (var method in type.GetMethods())
                {
                    Console.WriteLine(method.Name);
                    if (method.GetCustomAttributes(typeof(ProblemAttribute)) != null)
                    {
                        Console.WriteLine("Found a problem!");
                        if ((method.GetCustomAttributes(typeof(ProblemAttribute)) as ProblemAttribute).Problem == problem)
                        {
                            var output = method.Invoke(Activator.CreateInstance(type), null);
                            return output as string;
                        }
                    }
                }
            }
            */
            bool found = false;
            foreach (var type in types)
            {
                foreach (var method in type.GetMethods())
                {
                    var problemAttribute = method.GetCustomAttributes(typeof(ProblemAttribute), true);

                    if (problemAttribute != null && problemAttribute.Length > 0)
                    {
                        //TODO Handle multiple command match (more than one function matches the command)
                        foreach (ProblemAttribute attribute in problemAttribute)
                        {
                            if (attribute.Problem == problem)
                            {
                                found = true;
                                if (method.GetParameters().Length == 0)
                                {
                                    object output = method.Invoke(Activator.CreateInstance(type), null);
                                    return output.ToString();
                                }
                                Console.WriteLine("Your method should not have any parameters.");
                                found = false;
                            }
                        }
                    }
                }
            }
            if (!found)
                Console.WriteLine($"Failed to find a method identified as problem {problem}!");
            return "";
        }
    }
}
