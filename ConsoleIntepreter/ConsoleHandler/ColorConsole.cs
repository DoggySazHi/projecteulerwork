﻿using System;

namespace ProjectEuler.ConsoleIntepreter.ConsoleHandler
{
    public class ColorConsole
    {
        public static void WriteLine(string Text, string Color)
        {
            ConsoleColor[] colors = (ConsoleColor[])Enum.GetValues(typeof(ConsoleColor));
            bool colorFound = false;
            foreach (var color in colors)
            {
                if (color.ToString() == Color)
                {
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = color;
                    colorFound |= true;
                    break;
                }
            }
            Console.WriteLine(Text);
            Console.ResetColor();

            #if DEBUG
            //Warn!
            if (!colorFound)
                WriteLine("[WARN] Attempted to WriteLine with a color that doesn't exist!", ConsoleColor.Yellow);
            #endif
        }
        public static void WriteLine(string Text, ConsoleColor color)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = color;
            Console.WriteLine(Text);
            Console.ResetColor();
        }
    }
}
