﻿using System;
using System.Linq;
using System.Reflection;

namespace ProjectEuler.ConsoleIntepreter.ConsoleHandler
{
    public static class ConsoleCommands
    {
        public static void CommandInterpreter(string command, object[] parameters = null)
        {
            Console.WriteLine($"[CONSOLE issued command \"{command}\"]");

            bool found = false;
            string reason = "Command not found.";

            var types = from c in Assembly.GetExecutingAssembly().GetTypes()
                        where c.IsClass
                        select c;
            foreach (var type in types)
            {
                foreach (var method in type.GetMethods())
                {
                    var commandAttribute = method.GetCustomAttributes(typeof(CommandAttribute), true);

                    if (commandAttribute.Length > 0)
                    {
                        //TODO Handle multiple command match (more than one function matches the command)
                        foreach (CommandAttribute attribute in commandAttribute)
                        {
                            if (attribute.command.Equals(command, StringComparison.OrdinalIgnoreCase))
                            {
                                found = true;
                                if (parameters != null)
                                {
                                    if (method.GetParameters().Length == parameters.Length)
                                    {
                                        method.Invoke(Activator.CreateInstance(type), parameters);
                                        return;
                                    }
                                    else
                                    {
                                        found = false;
                                        reason = $"Command requires {method.GetParameters().Length} parameters.";
                                    }
                                }
                                else if (method.GetParameters().Length == 0)
                                {
                                    method.Invoke(Activator.CreateInstance(type), null);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            if (!found)
                Console.WriteLine(reason);
        }
    }
}
