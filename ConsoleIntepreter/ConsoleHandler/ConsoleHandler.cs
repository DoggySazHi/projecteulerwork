﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectEuler.ConsoleIntepreter.ConsoleHandler
{
    public static class ConsoleHandler
    {
        public static Stack<char> input = new Stack<char>();
        public static void CommandHandler()
        {
            WriteBottomLine($"> ");
            while (true)
            {
                string IO = CommandInput('>');
                string[] split = IO.Split(' ');
                if (split.Length > 1)
                    ConsoleCommands.CommandInterpreter(split[0], split.Skip(1).ToArray());
                else
                    ConsoleCommands.CommandInterpreter(split[0]);
            }
        }
        public static string CommandInput(char inputChar)
        {
            var toReturn = "";
            WriteBottomLine($"{inputChar} ");

            while (true)
            {
                ConsoleKeyInfo read = Console.ReadKey(true);
                char letter = read.KeyChar;
                ConsoleKey consoleKey = read.Key;
                if (consoleKey == ConsoleKey.Backspace && input.Count > 0)
                {
                    input.Pop();
                }
                else if (!(consoleKey == ConsoleKey.Enter))
                    input.Push(letter);

                //Refresh
                string output = "";
                List<char> updateList = new List<char>(input.Reverse());
                foreach (char i in updateList)
                {
                    output += i;
                }
                WriteBottomLine($"{inputChar} {output}");
                if (consoleKey == ConsoleKey.Enter && input.Count > 0)
                {
                    ClearLine();
                    toReturn = output;
                    input = new Stack<char>();
                    break;
                }
            }
            return toReturn;
        }
        public static void Reset()
        {
            input = new Stack<char>();
            WriteBottomLine("> ");
        }
        static void WriteBottomLine(string text)
        {
            int x = Console.CursorLeft;
            int y = Console.CursorTop;
            Console.CursorTop = Console.WindowTop + Console.WindowHeight - 1;
            ClearLine();
            Console.Write(text);
            Console.SetCursorPosition(x, y);
        }
        static void ClearLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }
    }
}
