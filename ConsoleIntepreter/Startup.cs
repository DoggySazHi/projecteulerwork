﻿using System;
using ProjectEuler.ConsoleIntepreter.Commands;

namespace ProjectEuler.ConsoleIntepreter
{
    public static class ConsoleStart
    {
        public static void Run()
        {
            Console.Title = "dotslash - By DoggySazHi";
            OuterMethod();
            ConsoleHandler.ConsoleHandler.CommandHandler();
        }

        public static void OuterMethod()
        {
            MOTD();
        }

        public static void MOTD()
        {
            Console.WriteLine("Welcome to dotslash!");
            Console.WriteLine($"Cloned at version 0.1.");
            Console.WriteLine($"Type ? or help for a list of commands.");
            Tester.LoadProblems();
            Console.WriteLine($"Ready!");
        }
    }
}
