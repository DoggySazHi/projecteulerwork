﻿using System;
namespace ProjectEuler
{
    //Can be only used on individual methods.
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ProblemAttribute : Attribute
    {
        public int Problem;

        public ProblemAttribute(int Problem)
            => this.Problem = Problem;
    }
}
