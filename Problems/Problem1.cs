﻿using System;
using System.Collections.Generic;

namespace ProjectEuler.Problems
{
    public class Problem1
    {
        [Problem(1)]
        public int Problem()
        {
            int counter = 0;
            for (int i = 1; i < 1000; i++)
                if (i % 3 == 0 || i % 5 == 0) counter += i;
            return counter;
        }

        [Problem(2)]
        public int Problem2()
        {
            Problem2(1, 2);
            return counter;
        }

        int counter = 2;

        private void Problem2(int a, int b)
        {
            if (b > 4000000)
                return;
            int sum = a + b;
            if (sum % 2 == 0)
            {
                counter += sum;
                Console.WriteLine(sum);
            }
            Problem2(b, sum);
        }

        [Problem(3)]
        public long Problem3()
        {
            long largest = 0;
            long n = 600851475143;
            while (true)
            {

                for (long i = 2; i <= n; i++)
                {
                    if (n % i == 0)
                    {
                        if (largest < i)
                            largest = i;
                        n /= i;
                        break;
                    }
                }
                if (n <= 1)
                    break;
            }
            return largest;
        }

        [Problem(4)]
        public int Problem4()
        {
            int max = 0;
            for (int j = 999; j > 99; j--)
            {
                for (int i = 999; i > 99; i--)
                {
                    string multiply = (j * i).ToString();
                    //tbh the reversing mechanism could be nicer
                    char[] charArray = multiply.Substring(multiply.Length / 2).ToCharArray();
                    Array.Reverse(charArray);
                    if (multiply.Substring(0, multiply.Length / 2) == new string(charArray))
                        if (j * i > max)
                        {
                            max = j * i;
                            Console.WriteLine($"Values: {j} and {i} out {max}");
                        }
                }
            }
            Console.WriteLine(max);
            return max;
        }

        [Problem(5)]
        public int Problem5()
        {
            return 11 * 13 * 7 * 2 * 2 * 17 * 3 * 3 * 19 * 2 * 2 * 5;
        }
        [Problem(6)]
        public ulong Problem6()
        {
            ulong sumsquare = 0;
            ulong squaresum = 0;
            for (ulong i = 1; i <= 100; i++)
            {
                sumsquare += (ulong)Math.Pow(i, 2);
                squaresum += i;
                Console.WriteLine($"Currently: {sumsquare} and {squaresum}");
            }
            Console.WriteLine(Math.Pow(squaresum, 2) - sumsquare);
            return (ulong)(Math.Pow(squaresum, 2) - sumsquare);
        }

        [Problem(7)]
        public int Problem7()
        {
            List<int> newPrimes = new List<int>();
            bool[] prime = new bool[10001 * 100 + 1];
            for (int i = 2; i <= 10001 * 100; i++) prime[i] = true;

            for (int i = 2; i <= 10001 * 100; i++)
            {
                if (prime[i])
                {
                    for (int j = i * 2; j <= 10001 * 100; j += i)
                        prime[j] = false;
                }
            }

            for (int i = 0; i < prime.Length; i++)
            {
                if (prime[i])
                {
                    Console.WriteLine(i);
                    newPrimes.Add(i);
                }
            }

            //note: arrays start at 0
            Console.WriteLine(newPrimes[10000]);
            return newPrimes[10000];

        }

        [Problem(8)]
        public long Problem8()
        {
            long max = 0;
            try
            {
                string bigmess = "7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450";
                if (bigmess.Length != 1000) Console.WriteLine("Failure!");
                int length = 13;
                for (int i = 0; i < bigmess.Length - length; i++)
                {
                    var split = bigmess.Substring(i, length);
                    long temp = 1;
                    foreach (var c in split)
                        temp *= long.Parse(c.ToString());
                    if (temp > max)
                        max = temp;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return max;
        }

        [Problem(9)]
        public int Problem9()
        {
            for (int i = 1; i < 1000; i++)
            {
                for (int j = 1; j < 1000; j++)
                {
                    Console.WriteLine($"{i}^2 + {j}^2 = {Math.Sqrt(Math.Pow(i, 2) + Math.Pow(j, 2))}^2");
                    if (Math.Sqrt(Math.Pow(i, 2) + Math.Pow(j, 2)) % 1 <= (double.Epsilon * 100))
                    {
                        Console.WriteLine("Pretty whole!");
                        if (Math.Abs(Math.Round(Math.Sqrt(Math.Pow(i, 2) + Math.Pow(j, 2))) + i + j - 1000) <= 0.1)
                            return (int)Math.Round(Math.Sqrt(Math.Pow(i, 2) + Math.Pow(j, 2))) * i * j;
                    }
                }
            }
            Console.WriteLine("Something failed?");
            return 0;
        }

        [Problem(10)]
        public ulong Problem10()
        {
            //Another PRIME PROBLEM.
            List<int> newPrimes = new List<int>();
            bool[] prime = new bool[2000002];
            for (int i = 2; i <= 2000001; i++) prime[i] = true;

            for (int i = 2; i <= 2000001; i++)
            {
                if (prime[i])
                {
                    for (int j = i * 2; j <= 2000001; j += i)
                        prime[j] = false;
                }
            }

            for (int i = 0; i < prime.Length; i++)
            {
                if (prime[i])
                {
                    Console.WriteLine(i);
                    newPrimes.Add(i);
                }
            }
            ulong counterer = 0;
            foreach (int p in newPrimes)
                counterer += ulong.Parse(p.ToString());
            return counterer;
        }

        [Problem(11)]
        public long Problem11()
        {
            long max = 0;

            string st = "08022297381500400075040507785212507791084949994017811857608717409843694804566200814931735579142993714067538830034913366552709523046011426924685601325671370236912231167151676389419236542240402866331380244732609903450244753353783684203517125032988128642367102638406759547066183864706726206802621220956394396308409166499421245558056673992697177878968314883489637221362309750076442045351400613397343133957817532822753167159403800462161409535692163905429635314755588824001754243629855786560048357189070544443744602158515417581980816805944769287392138652177704895540045208839735991607975732162626793327986688366887576220720346336746551232639353690442167338253911249472180846293240627636206936417230238834629969826759857404361620733529783190017431497148868116235705540170547183515469169233486143520189196748";
            int[,] num = new int[20, 20];
            int index = 0;
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    num[i, j] = int.Parse(st.Substring(index, 2));
                    index += 2;
                }
            }

            //right
            for (int i = 0; i < num.GetLength(0) - 4; i++)
            {
                for (int j = 0; j < num.GetLength(1); j++)
                {
                    long output = num[i, j] * num[i + 1, j] * num[i + 2, j] * num[i + 3, j];
                    if (output > max)
                        max = output;
                }
            }
            //down
            for (int i = 0; i < num.GetLength(0); i++)
            {
                for (int j = 0; j < num.GetLength(1) - 4; j++)
                {
                    long output = num[i, j] * num[i, j + 1] * num[i, j + 2] * num[i, j + 3];
                    if (output > max)
                        max = output;
                }
            }

            //downright
            for (int i = 0; i < num.GetLength(0) - 4; i++)
            {
                for (int j = 0; j < num.GetLength(1) - 4; j++)
                {
                    long output = num[i, j] * num[i + 1, j + 1] * num[i + 2, j + 2] * num[i + 3, j + 3];
                    if (output > max)
                        max = output;
                }
            }

            //downleft
            for (int i = 3; i < num.GetLength(0); i++)
            {
                for (int j = 0; j < num.GetLength(1) - 4; j++)
                {
                    int output = num[i, j] * num[i - 1, j + 1] * num[i - 2, j + 2] * num[i - 3, j + 3];
                    if (output > max)
                        max = output;
                }
            }

            return max;
        }

        [Problem(12)]
        public long Problem12()
        {
            //TODO multiply all numbers from 1-500, then see
            long index = 100000;
            long num = 0;
            var divisors = CountDivisors(num);
            while (divisors <= 500)
            {
                index++;
                num = CreateTriangleNum(index);
                divisors = CountDivisors(num);
                Console.WriteLine($"Index: {index} Number: {num} Divisors: {divisors}");
            }

            return num;
        }

        private long CreateTriangleNum(long count)
        {
            return (count + 1) * count / 2;
        }

        private long CountDivisors(long num)
        {
            // include itself
            long counter = 1;
            for(long i = 1; i <= num/2; i++)
                if (num % i == 0)
                    counter++;
            return counter;
        }
    }
}
