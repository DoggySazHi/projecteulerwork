﻿using ProjectEuler.ConsoleIntepreter;

namespace ProjectEuler
{
    class Program
    {
        static void Main()
        {
            ConsoleStart.Run();
        }
    }
}
